import QtQuick 2.10
import QtQuick.Controls 2.2
import QtQuick.Window 2.10
import DgQuick 1.0
import "content"

Window {
    id: window
    visible: true
    width: 1280
    height: 800
    title: qsTr("Hello World")

    SwipeView {
        id: swipeView
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: navigator.top
        currentIndex: 0
        Page1 {}
        Page2 {}
    }

    PageIndicator {
        count: swipeView.count
        currentIndex: swipeView.currentIndex
        anchors.bottom: swipeView.bottom
        anchors.horizontalCenter: swipeView.horizontalCenter
    }

    Navigator {
        id: navigator
        height: 200
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
    }

    EventHandler {
        objectName: "eventHandler"
        id: eventHandler
        anchors.fill: parent
        focus: true
    }

    ErrorHandler {
        objectName: "errorHandler"
        id: errorHandler
        anchors.fill: parent
        focus: true
        onError: {
            if(error === ErrorHandler.ErrorEyeTrackerSoftwareNotInstalled){
                errorDialog.message = "Tobii software not installed";
                errorDialog.visible = true
            }
            else if(error === ErrorHandler.ErrorEyeTrackerNotConnected){
                errorDialog.message = "Tobii eye tracker not connected";
                errorDialog.visible = true
            }
        }
    }

    Dialog {
        id: errorDialog
        property alias message : message.text
        title: "Error"
        x: (parent.width - width) / 2
        y: (parent.height - height) / 2
        width: 300
        height: 150
        standardButtons: DialogButtonBox.Ok
        Text {
            id: message
        }
    }
}
