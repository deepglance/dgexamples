#ifndef ERRORHANDLER_H
#define ERRORHANDLER_H

#include <qquickitem.h>

#include <memory>
using namespace std;

class ErrorHandler : public QQuickItem
{
    Q_OBJECT

public:
    enum Error
    {
        ErrorEyeTrackerSoftwareNotInstalled,
        ErrorEyeTrackerNotConnected};
    Q_ENUMS(Error)
    ErrorHandler(QQuickItem *parent = nullptr);
    ~ErrorHandler();

signals:
    void error(ErrorHandler::Error error);

private:
    struct ErrorHandlerImpl;
    unique_ptr<ErrorHandlerImpl> m_pImpl;
};

#endif // ERRORHANDLER_H
