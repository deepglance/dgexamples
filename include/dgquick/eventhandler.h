#ifndef EVENTFILTER_H
#define EVENTFILTER_H

#include "qquickitem.h"

class EventHandler : public QQuickItem
{
    Q_OBJECT

public:
    EventHandler();
    void mousePressEvent(QMouseEvent* event) override;
    void hoverMoveEvent(QHoverEvent* event) override;
    void keyPressEvent(QKeyEvent *event) override;
    void keyReleaseEvent(QKeyEvent *event) override;
    void wheelEvent(QWheelEvent *event) override;

signals:
    void mousePositionChanged(float x, float y);
    void mouseWheelChanged(float y);
    void keyPress(int key);
    void keyRelease(int key);
};

#endif // EVENTFHANDLER_H
