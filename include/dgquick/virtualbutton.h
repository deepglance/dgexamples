#ifndef VIRTUALBUTTON_H
#define VIRTUALBUTTON_H

#include <QtQuick/QQuickItem>

#include <memory>
using namespace std;

class VirtualButton: public QQuickItem
{
    Q_OBJECT
public:
    VirtualButton(QQuickItem *parent = Q_NULLPTR);
    ~VirtualButton() override;
    Q_INVOKABLE void press();
    Q_INVOKABLE void release();

private slots:
    void onObjectNameChanged(const QString &objectName);

private:
    struct VirtualButtonImpl;
    unique_ptr<VirtualButtonImpl> m_pImpl;
};

#endif // VIRTUALBUTTON_H
