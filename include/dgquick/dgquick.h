#ifndef DGQUICK_H
#define DGQUICK_H
#include <QtCore/QtGlobal>
#include <QQmlEngine>

#include "dgquick/eyearea.h"
#include "dgquick/eventhandler.h"
#include "dgquick/errorhandler.h"
#include "dgquick/trackstatus.h"
#include "dgquick/virtualbutton.h"

#ifdef _WIN32
    #if defined(DGQUICK_LIBRARY)
    #  define DGQUICK_EXPORT Q_DECL_EXPORT
    #else
    #  define DGQUICK_EXPORT Q_DECL_IMPORT
    #endif
#else
    #define DGQUICK_EXPORT
#endif

class QQmlApplicationEngine;

class DGQUICK_EXPORT DgQuick
{
public:
    enum EyeTrackerType
    {
        EyeTrackerSimulator,
        EyeTrackerTobii
    };
    DgQuick();
    void qmlRegisterTypes()
    {
        qmlRegisterType<EyeArea>("DgQuick", 1, 0, "EyeArea");
        qmlRegisterType<ErrorHandler>("DgQuick", 1, 0, "ErrorHandler");
        qmlRegisterType<EventHandler>("DgQuick", 1, 0, "EventHandler");
        qmlRegisterType<TrackStatus>("DgQuick", 1, 0, "TrackStatus");
        qmlRegisterType<VirtualButton>("DgQuick", 1, 0, "VirtualButton");
    }
    void start(EyeTrackerType eyeTrackerType);
};

#endif // DGQUICK_H
