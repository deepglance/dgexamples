#ifndef TRACKSTATUS_H
#define TRACKSTATUS_H

#include <QtQuick/QQuickPaintedItem>
#include <QColor>

#include <memory>
using namespace std;

class TrackStatus : public QQuickPaintedItem
{
    Q_OBJECT

public:
    TrackStatus(QQuickItem *parent = Q_NULLPTR);
    virtual ~TrackStatus() override;
    void paint(QPainter *painter) override;

signals:
    void rightPosition();
    void wrongPosition();

private:
    struct TrackStatusImpl;
    unique_ptr<TrackStatusImpl> m_pImpl;

};

#endif // TRACKSTATUS_H
