#ifndef EYEAREA_H
#define EYEAREA_H

#include <QtQuick/QQuickItem>
#include <QColor>

#include <memory>
using namespace std;

class EyeArea: public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(bool enabled READ enabled WRITE setEnabled)
    Q_PROPERTY(unsigned int eyeFocusDwellTime READ eyeFocusDwellTime WRITE setEyeFocusDwellTime)
    Q_PROPERTY(unsigned int actionDwellTime READ actionDwellTime WRITE setActionDwell)
    Q_PROPERTY(bool croppingEnabled READ croppingEnabled WRITE setCroppingEnabled)
    Q_PROPERTY(QString dragAndDropButtonName READ dragAndDropButtonName WRITE setDragAndDropButtonName)
    Q_PROPERTY(bool dragAndDropEnabled READ dragAndDropEnabled WRITE setDragAndDropEnabled)
    Q_PROPERTY(unsigned int focusGroup READ focusGroup WRITE setFocusGroup)
    Q_PROPERTY(ActionMode focusActionMode READ focusActionMode WRITE setFocusActionMode)
    Q_PROPERTY(QString focusButtonName READ focusButtonName WRITE setFocusButtonName)
    Q_PROPERTY(QRectF scrollDeadSubArea READ scrollDeadSubArea WRITE setScrollDeadSubArea)
    Q_PROPERTY(bool scrollEnabled READ scrollEnabled WRITE setScrollEnabled)
    Q_PROPERTY(bool zoomEnabled READ zoomEnabled WRITE setZoomEnabled)

public:
    enum ActionMode
    {
        ActionModeDwell,
        ActionModeButton
    };
    Q_ENUMS(ActionMode)
    EyeArea(QQuickItem *parent = nullptr);
    ~EyeArea() override;
    bool croppingEnabled() const;
    QString dragAndDropButtonName() const;
    QRectF scrollDeadSubArea() const;
    bool dragAndDropEnabled() const;
    ActionMode focusActionMode() const;
    QString focusButtonName() const;
    unsigned int actionDwellTime() const;
    bool enabled() const;
    unsigned int focusGroup() const;
    unsigned int eyeFocusDwellTime() const;
    bool scrollEnabled() const;
    bool zoomEnabled() const;
    void setCroppingEnabled(bool croppingEnabled);
    void setDragAndDropButtonName(QString name);
    void setDragAndDropEnabled(bool enabled);
    void setFocusGroup(unsigned int threshold);
    void setFocusActionMode(ActionMode mode);
    void setFocusButtonName(QString name);
    void setActionDwell(unsigned int actionProgressChanged);
    void setEnabled(bool enabled);
    void setEyeFocusDwellTime(unsigned int threshold);
    void setScrollDeadSubArea(QRectF rect);
    void setScrollEnabled(bool enabled);
    void setZoomEnabled(bool enabled);

signals:
    void action();
    void dragAndDropChanged(int eventToNotify, double dx, double dy);
    void actionProgressChanged(double actionProgress);
    void eyeFocusChanged(bool eyeFocus);
    void scrollChanged(double dx, double dy);
    void zoomChanged(double dz);

protected:
    void itemChange(ItemChange change, const ItemChangeData &value) override;

private:
    struct EyeAreaImpl;
    unique_ptr<EyeAreaImpl> m_pImpl;
};

#endif // EYEAREA_H
