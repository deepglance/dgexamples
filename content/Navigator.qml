import QtQuick 2.4

NavigatorForm {
    Connections {
        target: back
        onAction: {
            swipeView.currentIndex -= 1
        }
    }
    Connections {
        target: next
        onAction: {
            swipeView.currentIndex += 1
        }
    }
}
