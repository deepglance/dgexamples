import QtQuick 2.4
import DgQuick 1.0

EyeButtonForm {
   id: this_
   Connections {
        target: eyeArea
        onEyeFocusChanged: {
            this_.eyeFocus = eyeFocus;
        }
        onActionProgressChanged: {
            this_.actionProgress = actionProgress
        }
        onAction: {
            action();
        }
    }
}
