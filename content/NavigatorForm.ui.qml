import QtQuick 2.4

Rectangle {
    id: item1
    property alias back: back
    property alias next: next
    width: 1280
    height: 200
    color: "whitesmoke"

    EyeButton {
        id: back
        width: 150
        height: 150
        text: "Back"
        anchors.top: parent.top
        anchors.topMargin: 25
        anchors.left: parent.left
        anchors.leftMargin: 25
        enabled: swipeView.currentIndex != 0
    }

    EyeButton {
        id: next
        width: 150
        height: 150
        text: "Next"
        anchors.top: parent.top
        anchors.topMargin: 25
        anchors.right: parent.right
        anchors.rightMargin: 25
        enabled: swipeView.currentIndex != swipeView.count - 1
    }
}
