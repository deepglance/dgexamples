import QtQuick 2.4

Page1Form {
    Connections {
        target: button1
        onAction: {
            button1.selected = true
            button2.selected = false
        }
    }
    Connections {
        target: button2
        onAction: {
            button1.selected = false
            button2.selected = true
        }
    }
}
