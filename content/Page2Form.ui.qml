import QtQuick 2.4

Rectangle {
    property alias button3: eyeButton4
    property alias button4: eyeButton3
    width: 1280
    height: 600
    color: "white"

    Text {
        text: "Page 2"
        font.bold: false
        font.pixelSize: 40
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 65
        horizontalAlignment: Text.AlignHCenter
    }
    Row {
        y: 250
        anchors.horizontalCenter:  parent.horizontalCenter
        spacing: 100
        EyeButton {
            id: eyeButton3
            text: "Button 3"
        }
        EyeButton {
            id: eyeButton4
            text: "Button 4"
        }
    }
}
