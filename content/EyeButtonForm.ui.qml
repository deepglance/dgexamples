import QtQuick 2.4
import DgQuick 1.0

Rectangle {
    property alias eyeArea: eyeArea
    property alias text: label.text
    property bool eyeFocus: false
    property bool selected: false    
    property double actionProgress: 0
    property bool actionProgressFeedbackEnabled: true
    signal action

    color: enabled ? "#5a98ee" : "#AA5a98ee"
    width: 200
    height: 200

    Text {
        id: label
        color: enabled ? "#ffffff" : "#AAffffff"
        text: "Button"
        font.pixelSize: 30
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
    }

    Rectangle {
        id: actionDwellFeedbackack
        anchors.centerIn: parent
        z: 1
        width: parent.width * (1 - actionProgress)
        height: parent.height * (1 - actionProgress)
        color: "#55ffffff"
        radius: width * 0.5
        visible: actionProgressFeedbackEnabled && !selected && eyeFocus
    }

    Rectangle {
        id: selectionFrame
        anchors.fill: parent
        visible: actionProgressFeedbackEnabled && selected
        border.width: 8
        border.color: "lime"
        color: "#00000000"
    }

    EyeArea {
        id: eyeArea
        anchors.fill: parent
        enabled: parent.enabled
        actionDwellTime: 750
    }
}
