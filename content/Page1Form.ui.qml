import QtQuick 2.4
import DgQuick 1.0

Rectangle {
    property alias button1: eyeButton1
    property alias button2: eyeButton2
    width: 1280
    height: 600
    color: "white"
    Text {
        id: text1
        text: "Page 1"
        font.bold: false
        font.pixelSize: 40
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 50
        horizontalAlignment: Text.AlignHCenter
    }
    Row {
        y: 250
        anchors.horizontalCenter:  parent.horizontalCenter
        spacing: 100
        EyeButton {
            id: eyeButton1
            text: "Button 1"
        }
        EyeButton {
            id: eyeButton2
            text: "Button 2"
        }
    }
}
