import QtQuick 2.4

Page2Form {
    Connections {
        target: button3
        onAction: {
            button3.selected = true
            button4.selected = false
        }
    }
    Connections {
        target: button4
        onAction: {
            button3.selected = false
            button4.selected = true
        }
    }
}
