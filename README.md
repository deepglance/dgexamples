﻿# DGQuick Examples
 
Ready-to-use examples showing how to create an eye controlled QT Quick application using DGQuick. 
 
## Version 1.0.1 beta
 
Elements selection and navigation between pages using eye dwell actions
Creation of a custom "EyeButton" QML component
 
The example can be easily modified and extended using the QT Quick Designer visual editor. 
 
## Getting Started
 
These instructions will get you a copy of the project up and running on your local machine.
 
### Prerequisites
 
Supported eye trackers: 
- Tobii 4C eye tracker
- EyeTech Tm5 Mini (Coming soon)
 
Supported operating systems: 
- Windows
- macOS
- Linux
 
Other requirements:
- QT >= 5.11
- QT Creator >= 4.5.0

The provided DGQuick dynamic library was built with:
- Visual Studio 2017 on Windows
- XCode 9.3 on macOS
 
#### Installing Tobii Software on Windows 
 
- Download and install the Tobii Eye Tracking Core software:
[Tobii Eye Tracking Core](https://files.update.tech.tobii.com/Tobii_Eye_Tracking_Core_v2.13.3.7443_x86.exe)
 
- Connect the Tobii 4C eye tracker.
 
Follow the graphical steps to configure the eye tracker and to calibrate it.
 
#### Installing Tobii Software on OSX 
 
- Download end install the Tobii Eye Tracking Pro software:
[Tobii Eye Tracking Pro](https://files.update.tech.tobii.com/stream_engine/engine/macos/bundles/TobiiEyeTracking_0.3.2.890_pro.pkg)
- Connect the Tobii 4C eye tracker.
- Click on the “Tobii Eye Tracking Software” icon appeared in the top menu bar, status menu section.
- Go to “Preferences...”.
- Click the “Setup Display” and follow the graphical procedure.
- Click the “+” button to add and calibrate a new user.
 
### QT
 
- Get an open source or commercial version of QT, visiting:
https://www.qt.io/download
- Install QT version >= 5.11 and QT Creator version >= 4.5.0.
 
### Installing
 
- Clone the repository:
 
```
git clone https://gitlab.com/deepglance/dgexamples
```

- Open the project file with QT Creator:
```
dgexamples.pro
```
- Build it.
- Be sure that the eye tracker is connected.
- Run the project and enjoy it.
 
### DGQuick
 
DGQuick provides QML components designed to easily integrate eye tracking functionalities in a QT Quick application.
[DeepGlance Explorer](http://www.deepglance.com/deepglance-explorer-download) was entirely designed and developed using DGQuick using the QT Quick Designer visual editor. 
 
This example demonstrate how to use DGQuick.
 
- Instantiate the DGQuick library in the main.cpp:
```
DgQuick dgQuick
```
 - Register the QML types:
```
dgQuick.qmlRegisterTypes();
```
- Start the DeepGlance Quick:
```
dgQuick.start(DgQuick::EyeTrackerTobii);
```
 
### Eye tracker simulator
 
If you do not have an eye tracking device available you can use the eye tracker simulator modifying the following line in the main.cpp:
```
dgQuick.start(engine, DgQuick::EyeTrackerTobii);
```
To:
```
dgQuick.start(engine, DgQuick::EyeTrackerSimulator);
```
In this way the eye tracker is simulated through the mouse cursor. This mean that the application will react as if you are gazing on the point where the cursor is positioned.
 
## Authors
 
DeepGlance.com
 
## License
 
The source code of this project is licensed under the BSD license. Linking against the content of the ‘lib’ directory is permitted for not commercial products.
 
## References
 
[QT Quick](https://www1.qt.io/qt-quick/)
[QT Quick Designer](http://doc.qt.io/qtcreator/creator-using-qt-quick-designer.html)
